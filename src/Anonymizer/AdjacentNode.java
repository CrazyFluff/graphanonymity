package Anonymizer;

/**
 * Describes the connection between one node and another
 * @author Alex
 *
 */
public class AdjacentNode implements Cloneable {
	//Variables
	AdjacentNode next = null;
	int id;
	
	//Initializer
	AdjacentNode(int id) {
		this.id = id;
	}
	
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}
}
