package Anonymizer;

/**
 * An Edge List. Describes the all the connections one node has.
 * @author Alex
 *
 */
public class AdjacentNodeList implements Cloneable {
	//The first Anonymizer.AdjacentNode in the list
	AdjacentNode head = null;
	
	//Keeps track of how many adjacent nodes are currently in the list
	int count = 0;
	
	
	//Initializer
	AdjacentNodeList() {}
	
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}
	
	/**
	 * Deletes all the AdjacentNodes in the list
	 */
	void deleteAll() {
		//Current node
		AdjacentNode cNode = head;
		
		while (cNode != null) {
			//Move the head 
			head = head.next;
			
			//Remove reference to next node
			cNode.next = null;
			
			//Move up current node
			cNode = head;
		}
		
		//Reset the count
		count = 0;
	}
	
	/**
	 * Adds an Anonymizer.AdjacentNode to the beginning of the list
	 * @param node 
	 * The node to add to the list
	 */
	void addAdjacentNode(AdjacentNode node) {
		node.next = head;
		head = node;
		
		//Increase the count
		count += 1;
	}
	
	/**
	 * Returns a string describing the contents of the Anonymizer.AdjacentNodeList.
	 * 
	 */
	public String toString() {
		String str = "[";
		AdjacentNode cNode = head;
		
		if (cNode != null) {
			str += cNode.id;
			cNode = cNode.next;
		}
		
		
		while (cNode != null) {
			str += " " + cNode.id ;
			cNode = cNode.next;
		}
		
		str += "]";
		
		return str;
	}
}
