package Anonymizer;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Comparator;

/**
 * Describes the architecture of a system of connections using the following classes:
 * - Anonymizer.Node, Anonymizer.AdjacentNode, Anonymizer.AdjacentNodeList
 * @author Alex
 *
 */
public class Graph implements Cloneable {
	private int edgeCount = 0;
	
	ArrayList<Node> nodes;
	
	//Initializer
	Graph(int nodeCount) {
		//Initialize ArrayList
		nodes = new ArrayList<Node>(nodeCount);
		
		//Initialize id for every node in the list
		for (int i = 0; i < nodeCount; i++) {
			nodes.add(new Node(i));
		}
		
	}
	
	public Graph clone() throws CloneNotSupportedException {
		Graph g = (Graph) super.clone();
		ArrayList<Node> nodesCopy = new ArrayList<Node>();
		
		for (Node n : nodes) {
			nodesCopy.add((Node) n.clone());
		}
		g.nodes = nodesCopy;
		
		
		return g;
	}
	
	/**
	 * @return
	 * # of nodes in the graph
	 */
	int getNodeCount() {
		return nodes.size();
	}
	
	/**
	 * Getter for edgeCount. Can never be set outside of class.
	 * @return
	 * # of connections in the graph
	 */
	int getEdgeCount() {
		return edgeCount;
	}
	
	/**
	 * Given an id, it will return the correlating node.
	 * @param id
	 * ID of node being searched for
	 * @return
	 * Anonymizer.Node with given id or null if it cannot be found.
	 */
	Node getNode(int id) {
		if (id >= nodes.size()) {
			System.out.println("ERROR: getNode(int) returned null because of out of bounds exception");
			return null;
		} else {
			return nodes.get(id);
		}
	}
	
	/**
	 * Adds an edge between one node and another.
	 * @param srcID
	 * The id of the source node
	 * @param destID
	 * The id of the target node
	 */
	boolean addEdge(int srcID, int destID) {
		return getNode(srcID).addAdjacentNode(destID);
	}
	
	public void print() {
		//for (int i = 0; i<10; i++) {
			//System.out.println(nodes.get(i).toString());
		for (Node node : nodes) {
			System.out.println(node.toString());
			
		}
	}
	
	public void writeToFile(String filename) throws UnsupportedEncodingException, FileNotFoundException, IOException {
		try (Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(filename), "utf-8"))) {
				
			
			
			
			//-------------------- Print out # of nodes and edges
			int numEdges = 0;
			for (Node n : nodes) {
				numEdges += n.getActualDegree();
			}
			writer.write("# NODES: " + nodes.size() + "\t# EDGES: " + numEdges + "\n");
			
			//-------------------- Print out all degrees (sorted)
			int numPerLine = 50;
			ArrayList<Node> nodesCopy = (ArrayList<Node>) nodes.clone();
			
			nodesCopy.sort(new Comparator<Node>() {
		           
	            public int compare(Node lhs, Node rhs) {
	                // -1 - less than, 1 - greater than, 0 - equal, all inversed for descending
	                return lhs.getDegree() > rhs.getDegree() ? -1 : (lhs.getDegree() < rhs.getDegree()) ? 1 : 0;
	            }
	        });
			
			writer.write("SORTED DEGREES:\n[");
			writer.write(nodesCopy.get(0).getDegree());
			for (int i = 1; i < nodesCopy.size(); i++) {
				writer.write(" " + nodesCopy.get(i).getDegree());
				
				if (i%50 == 0) {
					writer.write("\n ");
				}
			}
			
			writer.write("]\n\n\n");
			
			//-------------------- Print out graph
			for (Node node : nodes) {
				writer.write(node.toString());
				writer.write("\n");
			}
		}
	}
}
