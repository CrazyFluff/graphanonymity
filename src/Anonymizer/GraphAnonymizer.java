package Anonymizer;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class GraphAnonymizer {

	//Space requirement is n^2 to calculate all I_values
	int I_values[][]; 
	int DA_values[];
	int splitLocations[];
	
	int DIFF = 1;

	private Scanner fileScanner;
	private static final Pattern EDGE_PATTERN = Pattern.compile(".*?(\\d+)\\s+(\\d+).*");
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		GraphAnonymizer GA = new GraphAnonymizer();
		
		try {
			Graph graph = GA.createGraph("Enron.txt");
			//Anonymizer.Graph graph = GA.createGraph("GraphSample1.txt");
			
			
			
//			ArrayList<Anonymizer.Node> d = GA.prepGraph(graph);
//			ArrayList<Anonymizer.Node> d_hat = GA.DP(d, 4);
//			Anonymizer.Graph possibleGraph = GA.superGraph(graph,d_hat);
//			
//			if (possibleGraph != null) {
//				possibleGraph.print();
//			} else {
//				System.out.println("Unknown Solution to Anonymizer.Graph Anonymization");
//			}
			
			Graph superGraph = GA.probe(graph, 4);
//			superGraph.print();
			superGraph.writeToFile("Enron_Output2.txt");
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	GraphAnonymizer() {}
	
	/**
	 * Preps the Anonymizer.GraphAnonymizer for the graph to be anonymized
	 * @param g
	 * The graph to anonymize
	 * @return
	 * An ArrayList of Nodes ordered by degree.
	 */
	private ArrayList<Node> prepGraph(Graph g) {
		int size = g.getNodeCount();
		System.out.println("Size: " + size);
		
		I_values = new int[size+1][size+1];
		DA_values = new int[size+1];
		splitLocations = new int[size+1];
		
		ArrayList<Node> d = (ArrayList<Node>) g.nodes.clone();
		d.sort(new Comparator<Node>() {
           
            public int compare(Node lhs, Node rhs) {
                // -1 - less than, 1 - greater than, 0 - equal, all inversed for descending
                return lhs.getDegree() > rhs.getDegree() ? -1 : (lhs.getDegree() < rhs.getDegree()) ? 1 : 0;
            }
        });
		
		d.add(0, new Node(-1));
		
		
		
		return d;
	}
	
	/**
	 * 
	 * @param fileName
	 * @return
	 * @throws FileNotFoundException
	 */
	Graph createGraph(String fileName) throws FileNotFoundException {
		File f = new File(fileName);
		fileScanner = new Scanner(f);
		
		Graph graph = null;
		
		while (fileScanner.hasNextLine()) {
			
			String line = fileScanner.nextLine();
			Matcher m = EDGE_PATTERN.matcher(line);
			if (m.matches()) {
				
			    int src = Integer.parseInt(m.group(1));
			    int dest =  Integer.parseInt(m.group(2));
			    
			    //If the graph is null, create a new graph 
			    if (graph == null) {
			    	System.out.println("Created new graph with node/edge count: " + src + "/" + dest);
			    	graph = new Graph(src);
			    } else {
			    	graph.addEdge(src, dest);
			    }
			    
			    
			} //End of regex match
		} //End of while loop
		
		//Close the file scanner
		fileScanner.close();
		
		graph.print();
		
		//System.out.println(graph.toString());
		
		return graph;
	}
	
	
    //================================================================================
    // Degree Anonymization Algorithms
    //================================================================================
	ArrayList<Node> DP(ArrayList<Node> d, int k) {
		System.out.println("\n----------[[DP ALGORITHM]]----------");
		
		//Reset degree values in d Array
		for (Node n : d) {
			n.additionalDegrees = 0;
			n.targetDegree = -1;
		}
		
		//--------------------------------------------------------------------
		// Pre-processing step described in Section 4 O(n^2)
		// The cost of assigning all nodes from i to j the same degree
		//--------------------------------------------------------------------
		
		for (int i = 1; i < d.size(); i++) {
			for (int j = i+1; j < d.size(); j++) {
				
				//TODO: Improve. Use previous values to calculate additional values
				//I_values[i][j] = I(d,i,j);
				
				//Improved version:
				
				I_values[i][j] = I_values[i][j-1] + (d.get(i).getDegree() - d.get(j).getDegree());
				
			}
		}
			
		//--------------------------------------------------------------------
		// Print out the I-values
		//--------------------------------------------------------------------
		
//		System.out.println("I_values: ");
//		for (int i = 1; i < graph.getNodeCount(); i++) {
//			//System.out.println();
//			for (int j = 1; j < graph.getNodeCount(); j++) {
//				System.out.print(I_values[i][j] + "\t");
//			}
//			System.out.println();
//		}
		
		
		//--------------------------------------------------------------------
		// Calculate the DA values (degree-anonymization cost of subsequence d[1,i])
		//--------------------------------------------------------------------
		
		for (int i = 1; i < d.size(); i++) {
			//When i is less than 2k 
			if (i < 2*k) {
				DA_values[i] = I_values[1][i];
			} else { 
				
				
				//When i greater than or equal to 2k
				int start = Math.max(k, i-2*k+1);
				int minValue = -1;
				
				//Go through at most k degrees, calculate the cost for change, and retain the minimum cost
				for (int t = start; t <= i-k; t++) {
					if (minValue == -1) {
						minValue = DA_values[t] + I_values[t+1][i];
						splitLocations[i] = t;
					} else {
						int smallest = Math.min(minValue, DA_values[t] + I_values[t+1][i]);
						
						if (minValue != smallest) {
							minValue = smallest;
							splitLocations[i] = t;
						}
						
					}
				}
				DA_values[i] = minValue;
			}
		}
		
		//--------------------------------------------------------------------
		// Print out the total cost to take into account each node
		//--------------------------------------------------------------------
		
//		System.out.print("\nDEGREE-ANONYMIZATION COSTS: [");
//		System.out.print(DA_values[1]);
//
//		for (int i = 2; i < d.size(); i++) {
//			System.out.print(" " + DA_values[i]);
//		}
//		System.out.print("]\n");
		
		//--------------------------------------------------------------------
		// Print out all the locations that splits should occur
		//--------------------------------------------------------------------
		
//		System.out.print("\nOPTIMAL SPLIT LOCATIONS: [");
//		System.out.print(splitLocations[1]);
//
//		for (int i = 2; i < d.size(); i++) {
//			System.out.print(" " + splitLocations[i]);
//		}
//		System.out.print("]\n");
		
		//--------------------------------------------------------------------
		// Adjust the degrees (d) to be k-anonymous
		//--------------------------------------------------------------------
		
		int endPoint = d.size()-1; 
		int startPoint = splitLocations[endPoint] + 1;
		int cumulativeChange = 0; //TODO: Remove
		
		while (endPoint != 0) {
			int highestDegree = d.get(startPoint).getDegree();
			
			//Start at startPoint+1 because the start point already has the highest degree of its section
			for (int i = startPoint+1; i <= endPoint; i++) {
				cumulativeChange += highestDegree - d.get(i).getDegree();
				d.get(i).targetDegree = highestDegree;
			}
			
			endPoint = startPoint-1;
			startPoint = splitLocations[endPoint] + 1;
		}
		
		//Assign the target degree for the remaining values
		for (int i = 1; i < d.size(); i++) {
			Node n = d.get(i);
			n.targetDegree = Math.max(n.getDegree(), n.targetDegree);
			n.additionalDegrees = n.targetDegree - n.getActualDegree();
		}

		
		//--------------------------------------------------------------------
		// Print out how many edges were added and the output
		//--------------------------------------------------------------------
		
//		System.out.print("# Edges to be Added (based on d): " + cumulativeChange);
		
		//IDS
		
//		System.out.print("\n-----------------------------------------------------\nIDs -> d: \t\t[");
//		System.out.print(d.get(1).id);
//
//		for (int i = 2; i < d.size(); i++) {
//			System.out.print(" " + d.get(i).id);
//		}
//		System.out.print("]\n");
		
		//INITIAL
		
//		System.out.print("\nINPUT -> d: \t\t[");
//		System.out.print(d.get(1).getDegree());
//
//		for (int i = 2; i < d.size(); i++) {
//			System.out.print(" " + d.get(i).getDegree());
//		}
//		System.out.print("]\n");
		
		
		//CHANGE IN D 
//		System.out.print("\nDELTA D -> delta_d: \t[");
//		System.out.print(d.get(1).targetDegree - d.get(1).getDegree());
//
//		for (int i = 2; i < d.size(); i++) {
//			System.out.print(" " + (d.get(i).targetDegree - d.get(i).getDegree()));
//		}
//		System.out.print("]\n");
		
		//CHANGE
		
//		System.out.print("\nACTUAL CHANGE -> add: \t[");
//		System.out.print(d.get(1).additionalDegrees);
//
//		for (int i = 2; i < d.size(); i++) {
//			System.out.print(" " + (d.get(i).additionalDegrees));
//		}
//		System.out.print("]\n");
		
		
		//FINAL
		
//		System.out.print("\nOUTPUT -> d_hat: \t[");
//		System.out.print(d.get(1).targetDegree);
//
//		for (int i = 2; i < d.size(); i++) {
//			System.out.print(" " + d.get(i).targetDegree);
//		}
//		System.out.print("]\n-----------------------------------------------------\n");
		
		//------------------------------------
		
		return d;
	}
	
//	//Calculates the cost of assigning all nodes [i,j] the same degree.
//	int I(ArrayList<Integer> d, int start, int end) {
//
//		
//		int largestDegree = d.get(start);
//		int cost = 0;
//		
//		for (int i = start; i <= end; i++) {
//			cost += largestDegree - d.get(i);
//		}
//		
//		return cost;
//	}
	
	void greedy() {
	
	}

    //================================================================================
    // Anonymizer.Graph Construction Algorithms
    //================================================================================
	
	Graph constructGraph(ArrayList<Node> d_hat) {
		int edgesAdded = 0;
		
		//Create a new graph 
		Graph graph = new Graph(d_hat.size()-1);
		
		//Get the largest degree
		int largestDegree = d_hat.get(1).getDegree();
		
		//Create a new array of hashtables and initialize it
		ArrayList<Hashtable<String, Node>> dArray = new ArrayList<Hashtable<String, Node>>(largestDegree+1);
		for (int i = 0; i < largestDegree+1; i++) {
			dArray.add(new Hashtable<String, Node>());
		}
		
		//Add nodes to corresponding positions in array
		for (int i = 1; i < d_hat.size(); i++) {
			Node n = d_hat.get(i);
			dArray.get(n.targetDegree).put(Integer.toString(n.id), n);
		}
		
		//Calculate the sum of the degrees
		int sumDegrees = 0;
		for (int i = 1; i < d_hat.size(); i++) {
			Node n = d_hat.get(i);
			sumDegrees += n.targetDegree;
		}
		
		//If the sum of the degrees is odd then 'Halt and return "No"'
		if (sumDegrees%2 == 1) {
			return null;
		} else {
			System.out.println("Total Edges to be Added: " + sumDegrees);
		}
		
		int prevLargestDegree = dArray.size()-1;
		
		//While true do...
		while (true) {
			
			//If the # of edges added equal the total # of degrees (the degree sequence d are all 0s) then 
			if (edgesAdded == sumDegrees) {
				return graph;
			}
			
			//Pick a node with the largest degree
			Node n = null;
			ArrayList<Node> adjNodes = new ArrayList<Node>();
			
			for (int i = Math.min(prevLargestDegree, dArray.size()-1); i > 0; i--) {
				Collection<Node> nodes = dArray.get(i).values();
				
				//If there exists nodes within this hashtable
				if (nodes.size() > 0) {
					Iterator<Node> iter = nodes.iterator();
					
					//A node hasn't been picked yet.
					if (n == null) {
						n = iter.next();
						prevLargestDegree = i;
					
					} 
					//A node has been picked. Pick d(v) other nodes.
					while (adjNodes.size() < n.targetDegree && iter.hasNext()) {
						adjNodes.add(iter.next());
					}
					
				} 
				
				//Found d(v)-highest entries in d, so break.
				if (n != null && adjNodes.size() == n.targetDegree) {
					break;
				}
			}

			//Set the degree of the picked node to 0 and move the value in the array of hashtables
			dArray.get(n.targetDegree).remove(Integer.toString(n.id));
			n.targetDegree = 0;
			dArray.get(0).put(Integer.toString(n.id), n);
			
			//For each node in adjNodes add the corresponding edge
			for (int i = 0; i < adjNodes.size(); i++) {
				Node adjNode = adjNodes.get(i);
				
				//Add an edge between the picked node and the adjacent node
				graph.addEdge(n.id, adjNode.id);
				graph.addEdge(adjNode.id, n.id);
				
				//Reduce degree of adjacent node by 1 and move accordingly
				dArray.get(adjNode.targetDegree).remove(Integer.toString(adjNode.id));
				adjNode.targetDegree -= 1;
				dArray.get(adjNode.targetDegree).put(Integer.toString(adjNode.id), adjNode);
				
				edgesAdded += 2;

				//If degree is negative, return null
				if (adjNode.targetDegree < 0) {
					return null;
				}
			}
			
		}
	}
	
	//================================================================================
    // Anonymizer.Graph Construction Algorithms
    //================================================================================
	
	Graph superGraph(Graph g, ArrayList<Node> d) {
		
		System.out.println("----------[[SUPERGRAPH ALGORITHM]]----------");
		
		int edgesAdded = 0;
		Graph graph;
		ArrayList<Node> d_hat = new ArrayList<Node>();
		try {
			graph = g.clone();
			for (int i = 0; i < d.size(); i++) {
				d_hat.add(i, (Node) d.get(i).clone());
			}
		} catch (CloneNotSupportedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
		


		
		//Sort the list by additional degrees
		d_hat.remove(0);
		d_hat.sort(new Comparator<Node>() {
            public int compare(Node lhs, Node rhs) {
                // -1 - less than, 1 - greater than, 0 - equal, all inversed for descending
                return lhs.additionalDegrees > rhs.additionalDegrees ? -1 : (lhs.additionalDegrees < rhs.additionalDegrees) ? 1 : 0;
            }
		});
		d_hat.add(0, new Node(-1));
		
		
		//Get the largest # of additional degrees to be added to one node
		int largestDegree = d_hat.get(1).additionalDegrees;
		
		//Create a new array of hashtables and initialize it
		ArrayList<Hashtable<String, Node>> dArray = new ArrayList<Hashtable<String, Node>>(largestDegree+1);
		for (int i = 0; i < largestDegree+1; i++) {
			dArray.add(new Hashtable<String, Node>());
		}
		
		//Add nodes to corresponding positions in array
		for (int i = 1; i < d_hat.size(); i++) {
			Node n = d_hat.get(i);
			dArray.get(n.additionalDegrees).put(Integer.toString(n.id), n);
		}
		
		//Calculate the sum of the degrees
		int sumDegrees = 0;
		for (int i = 1; i < d_hat.size(); i++) {
			Node n = d_hat.get(i);
			sumDegrees += n.additionalDegrees;
		}
		
		//If the sum of the degrees is odd then 'Halt and return "No"'
		if (sumDegrees%2 == 1) {
			System.out.println("ERROR (SuperGraph): Attempting to add odd # of edges");
			return null;
		} else {
			System.out.println("Total Edges to be Added: " + sumDegrees);
		}
		
		int prevLargestDegree = dArray.size()-1;
		
		//While true do...
		while (true) {
			
			//If the # of edges added equal the total # of degrees (the degree sequence d are all 0s) then 
			if (edgesAdded == sumDegrees) {
				return graph;
			}
			
			//Pick a node with the largest degree
			Node n = null;
			ArrayList<Node> adjNodes = new ArrayList<Node>();
			
			for (int i = Math.min(prevLargestDegree, dArray.size()-1); i > 0; i--) {
				Collection<Node> nodes = dArray.get(i).values();
				
				//If there exists nodes within this hashtable
				if (nodes.size() > 0) {
					Iterator<Node> iter = nodes.iterator();
					
					//A node hasn't been picked yet.
					if (n == null) {
						n = iter.next();
						prevLargestDegree = i;
					
					} 
					//A node has been picked. Pick d(v) other nodes.
					while (adjNodes.size() < n.additionalDegrees && iter.hasNext()) {
						Node possibleConnection = iter.next();
						
						//If there isn't already an edge between the two nodes, add it to adjNodes
						if (!n.hasAdjacentNode(possibleConnection.id)) {
							adjNodes.add(possibleConnection);
						}
					}
					
				} 
				
				//Found d(v)-highest entries in d, so break.
				if (n != null && adjNodes.size() == n.additionalDegrees) {
//					System.out.println("BREAK - COMPLETE");
					break;
				}
			}
			
			if (n == null) {
				System.out.println("ERROR: NODE IS NULL (THIS SHOULD NEVER OCCUR)");
				return null;
			}
			
			System.out.println("NODE ID: " + n.id + " \t || \tADDITIONAL DEGREES: " + n.additionalDegrees + " \t || \tARRAY SIZE: " + adjNodes.size());
			
			if (adjNodes.size() != n.additionalDegrees) {
				DIFF = n.additionalDegrees - adjNodes.size();
				System.out.println("ERROR: ADJNODE SIZE != ADDITIONAL DEGREES (SHOULD ONLY OCCUR IF A SIMPLE GRAPH IS NOT REALIZABLE)");
				return null;
			}
			
			

			//Set the degree of the picked node to 0 and move the value in the array of hashtables
			dArray.get(n.additionalDegrees).remove(Integer.toString(n.id));
			n.additionalDegrees = 0;
			dArray.get(0).put(Integer.toString(n.id), n);
			
			//For each node in adjNodes add the corresponding edge
			for (int i = 0; i < adjNodes.size(); i++) {
				Node adjNode = adjNodes.get(i);
				
				//Add an edge between the picked node and the adjacent node
				graph.addEdge(n.id, adjNode.id);
//				System.out.println("Added edges between " + n.id + " and " + adjNode.id);
				graph.addEdge(adjNode.id, n.id);
				
				//Reduce degree of adjacent node by 1 and move accordingly
				dArray.get(adjNode.additionalDegrees).remove(Integer.toString(adjNode.id));
				adjNode.additionalDegrees -= 1;
				dArray.get(adjNode.additionalDegrees).put(Integer.toString(adjNode.id), adjNode);
				
				edgesAdded += 2;

				//If degree is negative, return null
				if (adjNode.additionalDegrees < 0) {
					return null;
				}
			}
			
		}
	}
	
	void priority() {
		
	}
	
	void greedySwap() {
		
	}
	
    //================================================================================
    // Probing Algorithm
    //================================================================================
	
	Graph probe(Graph graph, int k) {
		int iterations = 1;
		
		ArrayList<Node> d = prepGraph(graph);
		
		ArrayList<Node> d_hat = DP(d, k);

		Graph sGraph = superGraph(graph, d_hat);

		while (sGraph == null) {
			d = addNoise(d);
			d_hat = DP(d, k);
			sGraph = superGraph(graph, d_hat);
			iterations++;
			
		}
		
		System.out.println("\n-----------------------------------------------------\nTOTAL # PROBE ITERATIONS: " + iterations);
		
		return sGraph;
	}

	ArrayList<Node> addNoise(ArrayList<Node> d) {
		d.remove(0);
		
//		Anonymizer.Node smallestDegreeNode = d.get(d.size()-1);
//		Anonymizer.Node secondSmallestDegreeNode = d.get(d.size()-2);
//		
//		int largestDegree = d.get(0).getDegree();
//		
//		System.out.println("LARGEST DEGREE: " + largestDegree);
//		System.out.println("SMALLEST NODE DEGREE: " + smallestDegreeNode.getDegree());
//		System.out.println("DIFF: " + Math.min(100, largestDegree - smallestDegreeNode.getDegree()));
//		
//		smallestDegreeNode.increaseDegree(Math.min(100, largestDegree - smallestDegreeNode.getDegree()));
//		//secondSmallestDegreeNode.increaseDegree(1);
//		
		
		
		for (int i = 1; i <= DIFF; i++) {
			Node n = d.get(d.size()-i);
			n.increaseDegree(1);
		}
		
		
		d.sort(new Comparator<Node>() {
	           
            public int compare(Node lhs, Node rhs) {
                // -1 - less than, 1 - greater than, 0 - equal, all inversed for descending
                return lhs.getDegree() > rhs.getDegree() ? -1 : (lhs.getDegree() < rhs.getDegree()) ? 1 : 0;
            }
        });
		
		d.add(0, new Node(-1));
		
		return d;
	}
}
