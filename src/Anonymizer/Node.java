package Anonymizer;

/**
 * Describes a single node on a graph.
 * @author Alex
 *
 */
public class Node implements Cloneable {
	//Variables
	int id = -1;
	
	//Used in ConstructGraph algorithm (Section 5.1)
	int targetDegree = -1;
	
	//Used in the SuperGraph algorithm (Section 5.2)
	int additionalDegrees = 0;
	
	private int currentDegree = 0;
	private AdjacentNodeList adj = new AdjacentNodeList();
	
	//Initializer
	Node(int id) {
		this.id = id;
	}
	
	public Node clone() throws CloneNotSupportedException {
		Node n = (Node) super.clone();
		
		n.adj = (AdjacentNodeList)adj.clone();
		
		return n;
	}
	
//	/**
//	 * Getter for id property. Cannot be set after initialized.
//	 * @return
//	 * id for node
//	 */
//	public int getID() {
//		return id;
//	}
	
	public void increaseDegree(int inc) {
		currentDegree += inc;
	}
	
	/**
	 * Getter for degree. Can never be set. Based on # of edges.
	 * @return
	 * # of connections to node
	 */
	public int getDegree() {
		return currentDegree;
	}
	
	public int getActualDegree() {
		return adj.count;
	}
	
	
	/**
	 * Creates a new Anonymizer.AdjacentNode based on the parameters given and adds it to the Anonymizer.AdjacentNodeList.
	 * @param id
	 * The id of the Anonymizer.AdjacentNode
	 * @return
	 * Returns whether the addition of the node was successful
	 */
	public boolean addAdjacentNode(int id) {
		if (!hasAdjacentNode(id)) {
			AdjacentNode newNode = new AdjacentNode(id);
			adj.addAdjacentNode(newNode);
			currentDegree++;
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * Checks whether a node with a given id is already connected to the current node.
	 * @param id
	 * The id of the Anonymizer.AdjacentNode
	 * @return
	 * Returns whether a node with the id given exists in the edge list
	 */
	public boolean hasAdjacentNode(int id) {
		//Current Anonymizer.Node
		AdjacentNode cNode = adj.head;
		
		while (cNode != null) {
			
			//Return true if id match
			if (cNode.id == id) {
				return true;
			}
			
			//Else move onto the next adjacentNode
			cNode = cNode.next;
		}
		
		//Return false if the edge list doesn't contain this node already.
		return false;
	}
	
	/**
	 * 
	 */
	public String toString() {
		String str = "" + id;
		
		str += "  :  " + adj.toString();
		
		return str;
	}
	
}
